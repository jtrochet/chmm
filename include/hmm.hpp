#pragma once
/*****************************************************************************
Constexpr Hidden Markov Model

Copyright (C) 2020 Josh Marshall

Licensed under the AGPLv3

Relicensing available on request
*****************************************************************************/



/** **************************************************************************
\file hmm.hpp

Hidden Markov Model Explanation:
A Hidden Markov Model (HMM) is a machine learning technique ammenable to both
online prediction of the most likely next state given a history of states with
associated probabilities, or offline prediction of a most likely explanation
of a series of hidden states given observations with associated probabilities.
There also exists a usage for sequence alignment with complex weightings, but
you probably don't want to use a HMM for that.

The premise behind HMMs are that all useful history is represented in the most
recent hidden state (or in some variants, states) and this history in
combination with a corrosponding observation can accurately predict a hidden
state in a series of hidden states.  The hidden states and obervations are
paired in a series.

How a HMM works is by exploiting a type of dynamic programming to find a
longest/heaviet path through a tree.  The dynamic programming aspect amounts
to having the exponentially growing tree fold over onto itself to create a
multi-tree which looks like a lattice or trellace.  To make this work, each
possible hidden state at each layer selects its most likely history.  The
other histories are not expanded since they must be suboptimal.

==============================================================================
ex)
observations
O --- i --- j --- k

Non-dynamic

S -+- A -+- A -+- A P(A|i->A|j->A|k) = 0.#
   |     |     |
   |     |     +- B P(A|i->A|j->B|k) = 0.#
   |     |     |
   |     |     +- C P(A|i->A|j->C|k) = 0.#
   |     |
   |     +- B -+- A P(A|i->B|j->A|k) = 0.#
   |     |     |
   |     |     +- B P(A|i->B|j->B|k) = 0.#
   |     |     |
   |     |     +- C P(A|i->B|j->C|k) = 0.#
   |     |
   |     +- C -+- A P(A|i->C|j->A|k) = 0.#
   |     |     |
   |     |     +- B P(A|i->C|j->B|k) = 0.#
   |     |     |
   |     |     +- C P(A|i->C|j->C|k) = 0.#
   |
   +- B -+- A -+- A P(B|i->A|j->A|k) = 0.#
   |     |     |
   |     |     +- B P(B|i->A|j->B|k) = 0.#
   |     |     |
   |     |     +- C P(B|i->A|j->C|k) = 0.#
   |     |
   |     +- B -+- A P(B|i->B|j->A|k) = 0.#
   |     |     |
   |     |     +- B P(B|i->B|j->B|k) = 0.#
   |     |     |
   |     |     +- C P(B|i->B|j->C|k) = 0.#
   |     |
   |     +- C -+- A P(B|i->C|j->A|k) = 0.#
   |     |     |
   |     |     +- B P(B|i->C|j->B|k) = 0.#
   |     |     |
   |     |     +- C P(B|i->C|j->C|k) = 0.#
   |
   +- C -+- A -+- A P(C|i->A|j->A|k) = 0.#
         |     |
         |     +- B P(C|i->A|j->B|k) = 0.#
         |     |
         |     +- C P(C|i->A|j->C|k) = 0.#
         |
         +- B -+- A P(C|i->B|j->A|k) = 0.#
         |     |
         |     +- B P(C|i->B|j->B|k) = 0.#
         |     |
         |     +- C P(C|i->B|j->C|k) = 0.#
         |
         +- C -+- A P(C|i->C|j->A|k) = 0.#
               |
               +- B P(C|i->C|j->B|k) = 0.#
               |
               +- C P(C|i->C|j->C|k) = 0.#

dynamic

S -+- A \-- A   -- A P(A|i->B|j->A|k) = 0.#
   |     \     /
   +- B   - B /--- B P(A|i->B|j->B|k) = 0.#
   |
   +- C -+- C ---- C P(C|i->C|j->C|k) = 0.#

==============================================================================

There is an initial state of probabilities for hidden states since there is
not yet a history, transition probabilities from one hidden state to another,
and an emission probability of each observation (keeping in mind that
`P(A|i) ~ P(i|A)` here).

*************************************************************************** */

/*****************************************************************************
TODO: Explain HMM and document intended useage, particularities.
TODO: Add runtime adapter for mapping more complex types to enums and
indexes.
TODO: class as initialized namespace + limit exposed functionality?
*****************************************************************************/

#include <algorithm>
#include <array>
#include <type_traits>
#include <vector>
#include <utility>
#include <cassert>
#include <numeric>
#include <execution>
#include <inttypes.h>
#include <cassert>
#include <tuple>
#include <type_traits>
#include <iostream>


#include <boost/integer.hpp>
#include <magic_enum.hpp>
//#define BMAVX2OPT
//#include "bm64.h"



namespace CHMM_INTERNAL {


  using std::array;
  using std::unordered_map;
  using std::vector;
  using std::transform;
  using std::make_pair;
  using std::pair;
  using std::tuple;
  using namespace magic_enum::ostream_operators;

  static_assert(magic_enum::is_magic_enum_supported);


  template<
    typename HIDDEN_STATE_TYPE,
    typename PROBABILITY_TYPE
  >
  using LATTICE_VERTEX = pair<HIDDEN_STATE_TYPE, PROBABILITY_TYPE>;


  template<
    typename HIDDEN_STATE_TYPE
  >
  using LATTICE_LAYER = array<HIDDEN_STATE_TYPE, magic_enum::enum_count<HIDDEN_STATE_TYPE>()>;


  template<
    typename HIDDEN_STATE_TYPE,
    typename PROBABILITY_TYPE
  >
  using LAYER_PROBABILITIES = array<PROBABILITY_TYPE, magic_enum::enum_count<HIDDEN_STATE_TYPE>()>;


  template<
    typename HIDDEN_STATE_TYPE
  >
  using LATTICE = vector<LATTICE_LAYER<HIDDEN_STATE_TYPE>>;


  // https://stackoverflow.com/a/23782326
  constexpr unsigned numberOfBits(unsigned x){
    return x < 2 ? x : 1+numberOfBits(x >> 1);
  }


#ifdef CHMM_DEBUG
  template<
    typename HIDDEN_STATE_TYPE>
  bool
  validate_lattice_value(
    const HIDDEN_STATE_TYPE &lattice_layer_value
  ){
    return std::any_of(
      magic_enum::enum_values<HIDDEN_STATE_TYPE>().begin(),
      magic_enum::enum_values<HIDDEN_STATE_TYPE>().end(),
      [lattice_layer_value] (const HIDDEN_STATE_TYPE &valid_value) -> bool { return lattice_layer_value == valid_value; }
    );
  }
#endif


#ifdef CHMM_DEBUG
  template<
    typename HIDDEN_STATE_TYPE>
  bool
  validate_lattice_layer(
    const LATTICE_LAYER<HIDDEN_STATE_TYPE> &lattice_layer
  ){
    return std::all_of(lattice_layer.begin(), lattice_layer.end(), validate_lattice_value<HIDDEN_STATE_TYPE>);
  }
#endif


#ifdef CHMM_DEBUG
  template<
    typename HIDDEN_STATE_TYPE>
  bool
  validate_lattice(
    const LATTICE<HIDDEN_STATE_TYPE> &lattice
  ){
    return std::all_of(lattice.begin(), lattice.end(), validate_lattice_layer<HIDDEN_STATE_TYPE>);
  }
#endif


  /*
  [observation][current hidden state][previous hidden state]
  */
  template<
    typename HIDDEN_STATE_TYPE,
    typename OBSERVATION_STATE_TYPE,
    typename PROBABILITY_TYPE
  >
  std::array<
    std::array<
      std::array<
        PROBABILITY_TYPE,
        magic_enum::enum_count<HIDDEN_STATE_TYPE>()
      >,
      magic_enum::enum_count<HIDDEN_STATE_TYPE>()
    >,
    magic_enum::enum_count<OBSERVATION_STATE_TYPE>()
  >
  precompute_combined_prob_matrixes(
    std::array<
      std::array<
        PROBABILITY_TYPE,
        magic_enum::enum_count<HIDDEN_STATE_TYPE>()
      >,
      magic_enum::enum_count<HIDDEN_STATE_TYPE>()
    > transition_matrix,
    std::array<
      std::array<
        PROBABILITY_TYPE,
        magic_enum::enum_count<HIDDEN_STATE_TYPE>()
      >,
      magic_enum::enum_count<OBSERVATION_STATE_TYPE>()
    > emission_matrix
  ){
    std::array<
      std::array<
        std::array<
          PROBABILITY_TYPE,
          magic_enum::enum_count<HIDDEN_STATE_TYPE>()
        >,
        magic_enum::enum_count<HIDDEN_STATE_TYPE>()
      >,
      magic_enum::enum_count<OBSERVATION_STATE_TYPE>()
    > combined_prob_matrix;

    for(auto obs : magic_enum::enum_values<OBSERVATION_STATE_TYPE>()){
      for(auto prev : magic_enum::enum_values<HIDDEN_STATE_TYPE>()){
        for(auto curr : magic_enum::enum_values<HIDDEN_STATE_TYPE>()){
          const auto obs_idx = magic_enum::enum_index<OBSERVATION_STATE_TYPE>(obs).value();
          const auto prev_state_idx = magic_enum::enum_index<HIDDEN_STATE_TYPE>(prev).value();
          const auto curr_state_idx = magic_enum::enum_index<HIDDEN_STATE_TYPE>(curr).value();
          combined_prob_matrix[obs_idx][curr_state_idx][prev_state_idx] = transition_matrix[prev_state_idx][curr_state_idx] * emission_matrix[obs_idx][curr_state_idx];
        }
      }
    }

    return combined_prob_matrix;
  }


  template<
    typename HIDDEN_STATE_TYPE,
    typename PROBABILITY_TYPE
  >
  void
  normalize_prob_vec_to_unity(
    LAYER_PROBABILITIES<HIDDEN_STATE_TYPE, PROBABILITY_TYPE> &layer_probs
  ){
    const auto prob_sum = std::reduce(std::execution::par_unseq, layer_probs.begin(), layer_probs.end(), PROBABILITY_TYPE(0), std::plus<>());

    std::transform(
      layer_probs.begin(),
      layer_probs.end(),
      layer_probs.begin(),
      [prob_sum](const PROBABILITY_TYPE &rel_prob) -> PROBABILITY_TYPE { return rel_prob/prob_sum; }
    );
  }


  template<
    typename HIDDEN_STATE_TYPE,
    typename OBSERVATION_STATE_TYPE,
    typename PROBABILITY_TYPE
  >
  LAYER_PROBABILITIES<HIDDEN_STATE_TYPE, PROBABILITY_TYPE>
  first_layer_of_lattice_probabilities(
    const OBSERVATION_STATE_TYPE &observed_coin_toss,
    const auto &emission_matrix,
    const auto &initial_state
  ){
    LAYER_PROBABILITIES<HIDDEN_STATE_TYPE, PROBABILITY_TYPE> next_layer_probs;

    for(auto hidden_state_option : magic_enum::enum_values<HIDDEN_STATE_TYPE>()){
      // new_lattice_layer[magic_enum::enum_index<HIDDEN_STATE_TYPE>(hidden_state_option).value()].first = hidden_state_option;
      PROBABILITY_TYPE starting_state_prob = initial_state.at(magic_enum::enum_index(hidden_state_option).value());
      PROBABILITY_TYPE emission_prob = emission_matrix.at(magic_enum::enum_index(observed_coin_toss).value()).at(magic_enum::enum_index(hidden_state_option).value());

      next_layer_probs[magic_enum::enum_index<HIDDEN_STATE_TYPE>(hidden_state_option).value()] = emission_prob*starting_state_prob;
    }
    return next_layer_probs;
  }


  // WARNING: This function requires primed input in layer_probs, obtained from first_layer_of_lattice_probabilities()
  template<
    typename HIDDEN_STATE_TYPE,
    typename OBSERVATION_STATE_TYPE,
    typename PROBABILITY_TYPE
  >
  LATTICE_LAYER<HIDDEN_STATE_TYPE>
  determine_next_layer_of_lattice(
    const LATTICE<HIDDEN_STATE_TYPE> &lattice,
    const std::array<std::array<PROBABILITY_TYPE, magic_enum::enum_count<HIDDEN_STATE_TYPE>()>, magic_enum::enum_count<HIDDEN_STATE_TYPE>()> &observation_specified_combined_matrix,
    LAYER_PROBABILITIES<HIDDEN_STATE_TYPE, PROBABILITY_TYPE> &layer_probs
  ){

#ifdef CHMM_DEBUG
    assert(validate_lattice(lattice));
#endif
    normalize_prob_vec_to_unity<HIDDEN_STATE_TYPE, PROBABILITY_TYPE>(layer_probs);


    using LATTICE_VERTEX_T = LATTICE_VERTEX<HIDDEN_STATE_TYPE, PROBABILITY_TYPE>;
    using LATTICE_LAYER_T = std::array<LATTICE_VERTEX_T, magic_enum::enum_count<HIDDEN_STATE_TYPE>()>;

    auto select_most_likely_past_state_for_given_current_state = [
      lattice,
      observation_specified_combined_matrix,
      layer_probs
    ](
      const size_t &given_state_index
    ) -> LATTICE_VERTEX_T {

      const auto observation_and_current_specified_combined_matrix = observation_specified_combined_matrix[given_state_index];

      LATTICE_LAYER_T possible_transition_paths;

#ifdef CHMM_DEBUG
      assert(validate_lattice_layer(magic_enum::enum_values<HIDDEN_STATE_TYPE>()));
#endif

      std::transform(
        magic_enum::enum_values<HIDDEN_STATE_TYPE>().begin(),
        magic_enum::enum_values<HIDDEN_STATE_TYPE>().end(),
        possible_transition_paths.begin(),
        [observation_and_current_specified_combined_matrix, layer_probs](const HIDDEN_STATE_TYPE& hdn_stt){
#ifdef CHMM_DEBUG
          assert(validate_lattice_value(hdn_stt));
#endif
          const auto idx = magic_enum::enum_index<HIDDEN_STATE_TYPE>(hdn_stt).value();
          auto tr = LATTICE_VERTEX_T( hdn_stt, observation_and_current_specified_combined_matrix.at(idx) * layer_probs[idx] );
#ifdef CHMM_DEBUG
          assert(validate_lattice_value(std::get<0>(tr)));
#endif
          return tr;
        }
      );

      auto to_return = *std::max_element(
        std::execution::par_unseq,
        possible_transition_paths.begin(),
        possible_transition_paths.end(),
        [](const LATTICE_VERTEX_T &lhs, const LATTICE_VERTEX_T &rhs) -> bool {
          return std::get<1>(lhs) < std::get<1>(rhs);
        }
      );

#ifdef CHMM_DEBUG
      assert(validate_lattice_value(std::get<0>(to_return)));
#endif
      return to_return;
    };

    LATTICE_LAYER<HIDDEN_STATE_TYPE> backlink_lattice_layer;
    //std::cout << "Uninitialized backlink layer is " << std::get<0>(backlink_lattice_layer) << ", " << std::get<1>(backlink_lattice_layer) << std::endl;

    for( auto hidden_state_option : magic_enum::enum_values<HIDDEN_STATE_TYPE>()){
#ifdef CHMM_DEBUG
      assert(validate_lattice_value(hidden_state_option));
#endif
      const auto hdn_stt_idx = magic_enum::enum_index<HIDDEN_STATE_TYPE>(hidden_state_option).value();
      const auto backlink_prob_pair = select_most_likely_past_state_for_given_current_state(hdn_stt_idx);
      backlink_lattice_layer[hdn_stt_idx] = std::get<0>(backlink_prob_pair);
      layer_probs[hdn_stt_idx] = std::get<1>(backlink_prob_pair);
    }
#ifdef CHMM_DEBUG
    assert(validate_lattice_layer(backlink_lattice_layer));
    assert(validate_lattice(lattice));
#endif

    return backlink_lattice_layer;
  }


  template<
    typename HIDDEN_STATE_TYPE,
    typename PROBABILITY_TYPE>
  pair<
    PROBABILITY_TYPE,
    vector<HIDDEN_STATE_TYPE>
  >
  select_maximum_path(
    const LATTICE<HIDDEN_STATE_TYPE> &lattice,
    const LAYER_PROBABILITIES<HIDDEN_STATE_TYPE, PROBABILITY_TYPE> &layer_probs
  ){

#ifdef CHMM_DEBUG
    assert(validate_lattice(lattice));
#endif

    if(lattice.size() == 0){
      return pair<PROBABILITY_TYPE, vector<HIDDEN_STATE_TYPE>>(PROBABILITY_TYPE(1.0), vector<HIDDEN_STATE_TYPE>());
    }

    const auto max_prob_idx = std::distance(
      layer_probs.begin(),
      std::max_element(
        layer_probs.begin(),
        layer_probs.end()
      )
    );

    auto final_path = vector<HIDDEN_STATE_TYPE>(lattice.size()+1);

    // This reverse iterator stuff it to avoid a call to std::reverse().
    auto fp_r_itr = final_path.rbegin();
    //auto tmp = lattice.data();
    *fp_r_itr = lattice.back().at(max_prob_idx);
    std::cout << "max prob index: " << max_prob_idx << std::endl;

    using namespace magic_enum::ostream_operators;

    for(auto itr = lattice.rbegin(); itr != lattice.rend(); ++itr){
      std::cout << "." << *fp_r_itr << std::endl;
      unsigned long hdn_stt_idx = magic_enum::enum_index<HIDDEN_STATE_TYPE>(*fp_r_itr).value();
      auto to_push_back = itr->at(hdn_stt_idx);
      ++fp_r_itr;
      *fp_r_itr = to_push_back;
    }

#ifdef CHMM_DEBUG
    assert(final_path.size() == lattice.size()+1);
#endif

    return std::make_pair(layer_probs[max_prob_idx], final_path);
  }

};



template<
  typename FIRST_DIMENSION_TYPE,
  typename SECOND_DIMENSION_TYPE,
  typename PROBABILITY_TYPE
>
constexpr bool validate_probability_table(
  const std::array<std::array<PROBABILITY_TYPE, magic_enum::enum_count<SECOND_DIMENSION_TYPE>()>, magic_enum::enum_count<FIRST_DIMENSION_TYPE>()> &to_check
){
  static_assert(std::is_floating_point_v<PROBABILITY_TYPE>);
  static_assert(std::is_enum_v<FIRST_DIMENSION_TYPE>);
  static_assert(std::is_enum_v<SECOND_DIMENSION_TYPE>);

  if(to_check.size() == 0)
    return false;
  if(to_check[0].size() == 0)
    return false;

  auto error_bound = ((to_check.at(0).size() * 0.5) * std::numeric_limits<PROBABILITY_TYPE>::epsilon());
  for(size_t i = 0; i < magic_enum::enum_count<SECOND_DIMENSION_TYPE>(); ++i){
    auto add_from_row = [i] (const PROBABILITY_TYPE &accum, const auto &rhs_row) -> PROBABILITY_TYPE { return accum + rhs_row.at(i); };
    auto col_sum = std::accumulate(to_check.begin(), to_check.end(), PROBABILITY_TYPE(0.0), add_from_row);
    if(1.0 - error_bound >= col_sum)
      return false;
    if(col_sum >= 1.0 + error_bound)
      return false;
  }

  return true;
}


/*! Determine the most likely explanation of hidden states given a series of
observations and probability matrixes.

  \param initial_state - an array where the corrosponding index in the array
  represents the probability of the corrosponding ordered enum value's
  probability.  The sum of all values should be indistinguishable from 1.

  \param transision_probabilities - a 2D array with the outtermost index
  representing the prior state, the innermost index representing the current
  state, and the value stored at that location indicating the probability of
  that transition as a decimal in the range of [0, 1].  Each sum of
  [*][CONSTANT] should be indistinguishable from 1.
  [potential current hidden state][previous hidden state]

  \param emission_probabilities - a 2D array with the outtermost index
  representing a possible, currently selected hidden state and the innermost
  index representing the observation.  The value at this location must be a
  decimal in the range of [0, 1].  Each sum of [*][CONSTANT] should be
  indistinguishable from 1.  [observation][potential current hidden state]

  \param observation_series - a 1D starting and ending ordinal container
  holding a series of observations which are to be paired to an explanatory
  matching series of most likely hidden states.

  TODO: generate matrix A to reduce probability lookup time.
*/
template<
  typename HIDDEN_STATE_TYPE,
  typename OBSERVATION_STATE_TYPE,
  typename PROBABILITY_TYPE,
  typename OBERVATION_CONTAINER
>
std::vector<HIDDEN_STATE_TYPE>
hmm(
  const std::array<PROBABILITY_TYPE, magic_enum::enum_count<HIDDEN_STATE_TYPE>()> &initial_state,
  const std::array<std::array<PROBABILITY_TYPE, magic_enum::enum_count<HIDDEN_STATE_TYPE>()>, magic_enum::enum_count<HIDDEN_STATE_TYPE>()> &transision_probabilities,
  const std::array<std::array<PROBABILITY_TYPE, magic_enum::enum_count<HIDDEN_STATE_TYPE>()>, magic_enum::enum_count<OBSERVATION_STATE_TYPE>()> &emission_probabilities,
  const OBERVATION_CONTAINER &observation_series
){
  static_assert(std::is_floating_point_v<PROBABILITY_TYPE>);
  static_assert(std::is_enum_v<HIDDEN_STATE_TYPE>);
  static_assert(std::is_enum_v<OBSERVATION_STATE_TYPE>);

#ifdef CHMM_DEBUG
  assert(initial_state.size() >= 1);
  assert(observation_series.size() >= 1);

  // Validate the inputs.  These are domain requirements which can't easily be expressed in the type system yet.
  auto initial_state_error_bound = ((initial_state.size() * 0.5) * std::numeric_limits<PROBABILITY_TYPE>::epsilon());
  assert(1.0 - initial_state_error_bound < std::accumulate(initial_state.begin(), initial_state.end(), 0.0));
  assert(std::accumulate(initial_state.begin(), initial_state.end(), 0.0) < 1.0 + initial_state_error_bound);

  validate_probability_table<HIDDEN_STATE_TYPE, HIDDEN_STATE_TYPE, PROBABILITY_TYPE>(transision_probabilities);
  validate_probability_table<OBSERVATION_STATE_TYPE, HIDDEN_STATE_TYPE, PROBABILITY_TYPE>(emission_probabilities);
#endif

  // This generates the hidden state lattice/multi-tree and an array with the
  // final probabilities.
  // TODO: return the index the the maximum probability choice for the end of
  // the lattice in order to reduce amount of data moving.
  // TODO: Optimize index data size type to match the number of hidden states,
  // obervation types, number of observations.
  CHMM_INTERNAL::LATTICE<HIDDEN_STATE_TYPE> lattice;
  lattice.reserve(observation_series.size());
  CHMM_INTERNAL::LAYER_PROBABILITIES<HIDDEN_STATE_TYPE, PROBABILITY_TYPE> layer_probs = CHMM_INTERNAL::first_layer_of_lattice_probabilities<HIDDEN_STATE_TYPE, OBSERVATION_STATE_TYPE, PROBABILITY_TYPE>(
    observation_series[0],
    emission_probabilities,
    initial_state
  );

  auto combined_prob_matrix = CHMM_INTERNAL::precompute_combined_prob_matrixes<HIDDEN_STATE_TYPE, OBSERVATION_STATE_TYPE, PROBABILITY_TYPE>(transision_probabilities, emission_probabilities);

  for(auto o_itr = observation_series.begin()+1; o_itr != observation_series.end(); ++o_itr){
#ifdef CHMM_DEBUG
    assert(CHMM_INTERNAL::validate_lattice(lattice));
#endif
    lattice.push_back(
      CHMM_INTERNAL::determine_next_layer_of_lattice<
        HIDDEN_STATE_TYPE,
        OBSERVATION_STATE_TYPE,
        PROBABILITY_TYPE
      >(
        lattice,
        combined_prob_matrix[magic_enum::enum_index<OBSERVATION_STATE_TYPE>(*o_itr).value()],
        layer_probs
      )
    );
#ifdef CHMM_DEBUG
    assert(CHMM_INTERNAL::validate_lattice(lattice));
#endif
  }

  //print_graph_to_stdout<HIDDEN_STATE_TYPE, OBSERVATION_STATE_TYPE, PROBABILITY_TYPE, OBERVATION_CONTAINER>(lattice, observation_series);

  // Track back and select out the most likely path.
  // TODO: There might be a way to optimize memory usage here.
  return CHMM_INTERNAL::select_maximum_path<
    HIDDEN_STATE_TYPE,
    PROBABILITY_TYPE
  >(
    lattice,
    layer_probs
  ).second;
}
